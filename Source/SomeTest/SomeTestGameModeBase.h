// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SomeTestGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SOMETEST_API ASomeTestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

};
